﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Mitto.DataLayer.Constants;
using Mitto.Models;

namespace Mitto.DataLayer
{
    public class SMSRepository : ModelRepository<SMS>
    {
        
        public SMSRepository(MittoDbContext dbContext, ILoggerFactory loggerFactory) : base(dbContext, loggerFactory)
        {
        }

        public async Task<IList<SMS>> GetSentSMS(DateTime dateFrom, DateTime dateTo, int skipRecords = DefaultValues.SkipRecords, int takeRecords = DefaultValues.PageSize)
        {
            return await Exec(
                () => GetAll()
                        .Where(m => m.Date >= dateFrom && m.Date <= dateTo)
                        .Include(m => m.Sender)
                        .Include(m => m.Receiver)
                        .ThenInclude(p => p.Country)
                        .Skip(skipRecords)
                        .Take(takeRecords)
                        .OrderByDescending(m => m.Date)
                        .ToListAsync(),
                nameof(GetSentSMS),
                false);
        }

        public async Task<IList<StatisticItem>> GetStatisticInfo(DateTime dateFrom, DateTime dateTo, IEnumerable<int> mccItems = null)
        {
            return await Exec(
                () => GetAll()
                        .Where(m => m.Date >= dateFrom && m.Date <= dateTo && (mccItems == null || mccItems.Contains(m.Receiver.Country.MobileCode)))
                        .GroupBy(m => new
                        {
                            day = m.Date.ToString(DateFormats.Date),
                            mcc = m.Receiver.Country.MobileCode
                        })
                        .Join(
                            DbContext.Set<Country>(), 
                            gr => gr.Key.mcc, 
                            c => c.MobileCode, 
                            (gr, c) => new StatisticItem
                            {
                                Day = gr.Key.day,
                                Mcc = gr.Key.mcc,
                                PricePerSMS = c.Price,
                                Count = gr.Count(),
                                TotalPrice = gr.Sum(m => m.Receiver.Country.Price)
                            })
                        .OrderByDescending(si => si.Day)
                        .ThenBy(si => si.Mcc)
                        .ToListAsync(),
                nameof(GetSentSMS),
                false);
        }
    }
}
