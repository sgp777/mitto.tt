﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Mitto.Models;

namespace Mitto.DataLayer
{
    public class MittoDbContext : DbContext
    {
        private const string DefaultConnectionName = "DefaultConnection";

        public const short GermanMCC = 262;
        public const short AustriaMCC = 232;
        public const short PolandMCC = 260;

        public IConfiguration Configuration { get; }

        public ILoggerFactory LoggerFactory { get; }

        public DbSet<SMS> SMSs { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<Person> Persons { get; set; }
        
        public MittoDbContext(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            Configuration = configuration;
            LoggerFactory = loggerFactory;

            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLoggerFactory(LoggerFactory).EnableSensitiveDataLogging()
                .UseSqlServer(Configuration.GetConnectionString(DefaultConnectionName));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>()
                .HasOne(p => p.Country)
                .WithMany(c => c.Persons)
                .HasForeignKey(p => p.CountryID);

            modelBuilder.Entity<SMS>()
                .HasOne(m => m.Receiver)
                .WithMany(p => p.ReceivedSMSs)
                .HasForeignKey(m => m.ReceiverID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<SMS>()
                .HasOne(m => m.Sender)
                .WithMany(p => p.SentSMSs)
                .HasForeignKey(m => m.SenderID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<SMS>()
                .HasIndex(m => m.Date)
                .IsUnique(false);

            modelBuilder.Entity<Country>()
                .HasIndex(c => c.MobileCode)
                .IsUnique(true);

            modelBuilder.Entity<Country>().HasData(
            new Country[]
            {
                new Country { ID = 1, Name="Germany", MobileCode = GermanMCC, Code = 49, Price = 0.055M },
                new Country { ID = 2, Name="Austria", MobileCode = AustriaMCC, Code = 43, Price = 0.053M },
                new Country { ID = 3, Name="Poland", MobileCode = PolandMCC, Code = 48, Price = 0.032M }
            });
        }
    }
}
