using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using IModel = Mitto.Models.Interfaces.IModel;

namespace Mitto.DataLayer
{
    public abstract class ModelRepository<TModel> where TModel : class, IModel
    {
        protected ILogger Logger { get; set; }

        protected MittoDbContext DbContext { get; set; }

        protected DbSet<TModel> WorkSet => DbContext.Set<TModel>();

        protected ModelRepository(MittoDbContext dbContext, ILoggerFactory loggerFactory)
        {
            DbContext = dbContext;
            Logger = loggerFactory.CreateLogger(GetType());
        }

        protected async Task<TResult> Exec<TResult>(Func<Task<TResult>> resultFunc, string actionName, bool useTransactionScope = true)
        {
            var result = default(TResult);

            var scope = useTransactionScope ? new TransactionScope(TransactionScopeAsyncFlowOption.Enabled) : null;

            try
            {
                result = await resultFunc();

                if (scope != null)
                    scope.Complete();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"Error raises during execution of {actionName} operation. The detail message - {ex.Message}.");

                throw;
            }
            finally
            {
                if (scope != null)
                    scope.Dispose();
            }

            return result;
        }

        protected async Task Exec(Action resultAction, string actionName, bool useTransactionScope = true)
        {
            await Exec<object>(() =>
            {
                resultAction();
                return null;
            }, actionName, useTransactionScope);
        }

        protected virtual IQueryable<TModel> GetAll()
        {
            return WorkSet.AsNoTracking();
        }

        public async Task<TModel> GetById(long id)
        {
            return await Exec(() => WorkSet.FindAsync(id), nameof(GetById), false);
        }

        public async Task<TModel> SaveModel(TModel model)
        {
            return await Exec(async () =>
            {
                InnerSaveModel(model);

                await DbContext.SaveChangesAsync();
                await DbContext.Entry(model).GetDatabaseValuesAsync();

                return model;
            },
            nameof(SaveModel));
        }        

        public async Task<int> SaveModels(List<TModel> models)
        {
            return await Exec(async () =>
            {
                models.ForEach(m => InnerSaveModel(m));
                                
                return await DbContext.SaveChangesAsync();
            },
            nameof(SaveModels));
        }

        public async Task<bool> DeleteModel(long modelId)
        {
            return await Exec(() => InnerDeleteModel(modelId), nameof(DeleteModel));
        }

        protected virtual TModel InnerSaveModel(TModel model)
        {
            if (model.ID.HasValue)
            {
                var existModel = WorkSet.Find(model.ID);
                if (existModel != null)
                {
                    var existModelEntry = DbContext.Entry(existModel);
                    existModelEntry.State = EntityState.Detached;
                    WorkSet.Update(model);

                    return model;
                }
                else
                    model.ID = null;
            }

            WorkSet.Add(model);
            return model;
        }

        protected virtual async Task<bool> InnerDeleteModel(long modelId)
        {
            var existModel = await WorkSet.FindAsync(modelId);
            if (existModel != null)
            {
                var dbModelEntry = DbContext.Entry(existModel);
                dbModelEntry.State = EntityState.Deleted;

                await DbContext.SaveChangesAsync();
                return true;
            }
            else
                return false;
        }        
    }
}
