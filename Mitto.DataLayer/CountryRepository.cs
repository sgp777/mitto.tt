﻿using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Mitto.Models;

namespace Mitto.DataLayer
{
    public class CountryRepository : ModelRepository<Country>
    {
        public CountryRepository(MittoDbContext dbContext, ILoggerFactory loggerFactory) : base(dbContext, loggerFactory)
        {
        }

        public new async Task<Country[]> GetAll()
        {
            return await Exec(() => base.GetAll().ToArrayAsync(), nameof(GetAll), false);
        }
    }
}
