﻿namespace Mitto.DataLayer.Constants
{
    public static class DefaultValues
    {
        public const int SkipRecords = 0;

        public const int PageSize = 100;
    }
}
