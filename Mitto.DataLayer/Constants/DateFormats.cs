﻿namespace Mitto.DataLayer.Constants
{
    public static class DateFormats
    {
        public const string Date = "yyyy-MM-dd";

        public const string DateTime = "yyyy-MM-ddTHH:mm:ss";
    }
}
