﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Mitto.Models;

namespace Mitto.DataLayer
{
    public class PersonRepository : ModelRepository<Person>
    {
        public PersonRepository(MittoDbContext dbContext, ILoggerFactory loggerFactory) : base(dbContext, loggerFactory)
        {
        }

        public async Task<Person> GetPerson(string personName)
        {
            return await Exec(async () =>
            {
                Person result;
                var persons = await GetAll()
                                    .Where(p => p.Name == personName && p.Name.Contains(personName))
                                    .Include(p => p.Country)
                                    .ToListAsync();

                if (persons.Any())
                {
                    if (persons.Count == 1)
                        result = persons.First();
                    else
                        throw new Exception($"Some persons were found by '{ personName }' name");
                }
                else
                    throw new Exception($"Person is not found by '{ personName }' name");

                return result;
            },
            nameof(GetPerson),
            false);
        }
    }
}
