Notes:

1. Solution uses .Net Core 2.1.

2. The Code First approach is used to create using database. 
   By default the following connection string is used - "Server=(localdb)\\MSSQLLocalDB;Database=Mitto;Trusted_Connection=True;"
   It is declared in appsettings.json of Mitto.Service project.
   So for correct default running it is necessary to have local MSSQL Server else please provide valid MSSQL Server host in appsettings.json.

3. JmSoft.NetCore.FileLogger package is used to write logs in Mitto.log file. This is my own Nuget package. You can find its sources by following link - https://gitlab.com/sgp777/jmsoftlib.git



Deployment steps:

1. Rebuild solution in VS 2017.

2. Open "Test->Windows->Test Explorer" from VS main menu and run all found integration tests.

3. All Tests should be executed successfully.
