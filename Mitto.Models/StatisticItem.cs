﻿namespace Mitto.Models
{
    public class StatisticItem
    {
        public string Day { get; set; }

        public short Mcc { get; set; }

        public decimal PricePerSMS { get; set; }

        public int Count { get; set; }

        public decimal TotalPrice { get; set; }
    }
}
