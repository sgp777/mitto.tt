﻿using System;
using Mitto.Models.Interfaces;

namespace Mitto.Models
{
    public class SMS : IModel
    {
        public long? ID { get; set; }

        public long SenderID { get; set; }

        public long ReceiverID { get; set; }
        
        public Person Sender { get; set; }

        public Person Receiver { get; set; }

        public string Text { get; set; }

        public DateTime Date { get; set; }
    }
}
