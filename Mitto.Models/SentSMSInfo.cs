﻿using Mitto.Models.Enums;
using System.Collections.Generic;

namespace Mitto.Models
{
    public class SentSMSInfo
    {
        public class Item
        {
            public short Mcc { get; set; }

            public string From { get; set; }

            public string To { get; set; }

            public decimal Price { get; set; }
        }

        public int TotalCount { get; set; }

        public IEnumerable<Item> Items { get; set; }

        public ResultState State { get; set; }
    }
}
   