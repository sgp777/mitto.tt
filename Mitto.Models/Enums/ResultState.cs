﻿namespace Mitto.Models.Enums
{
    public enum ResultState
    {
        Failed,
        Success,
    }
}
