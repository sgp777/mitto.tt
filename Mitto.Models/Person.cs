﻿using System.Collections.Generic;

using Mitto.Models.Interfaces;


namespace Mitto.Models
{
    public class Person : IModel
    {
        public long? ID { get; set; }

        public long CountryID { get; set; }

        public Country Country { get; set; }

        public string Name { get; set; }

        public List<SMS> ReceivedSMSs { get; set; }

        public List<SMS> SentSMSs { get; set; }
    }
}
