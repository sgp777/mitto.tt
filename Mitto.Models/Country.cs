﻿using Mitto.Models.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mitto.Models
{
    public class Country : IModel
    {
        public long? ID { get; set; }

        public string Name { get; set; }

        public byte Code { get; set; }

        public short MobileCode { get; set; }

        [Column(TypeName = "decimal(18, 4)")]
        public decimal Price { get; set; }

        public List<Person> Persons { get; set; }
    }
}
