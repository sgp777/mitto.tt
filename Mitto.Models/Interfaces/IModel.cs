﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mitto.Models.Interfaces
{
    /// <summary>
    /// Common model interface
    /// </summary>
    public interface IModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        long? ID { get; set; }
    }
}
