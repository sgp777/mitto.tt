﻿using System;
using System.Net;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Mitto.Service.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private RequestDelegate Next { get; set; }

        private ILogger<ErrorHandlingMiddleware> Logger { get; set; }

        public ErrorHandlingMiddleware(RequestDelegate next, ILogger<ErrorHandlingMiddleware> logger)
        {
            Logger = logger;
            Next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await Next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            Logger.LogError(ex, "Request processing error");

            var code = (ex is UnauthorizedAccessException) ? HttpStatusCode.Unauthorized : HttpStatusCode.BadRequest;
            
            var result = JsonConvert.SerializeObject(new { error = ex.Message });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            return context.Response.WriteAsync(result);
        }
    }
}
