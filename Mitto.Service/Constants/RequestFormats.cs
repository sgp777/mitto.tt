﻿namespace Mitto.Service.Constants
{
    public static class RequestFormats
    {
        public const string Json = "json";

        public const string Xml = "xml";
    }
}
