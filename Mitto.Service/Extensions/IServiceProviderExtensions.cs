﻿using System;

namespace Mitto.Service.Extensions
{
    public static class IServiceProviderExtensions
    {
        public static TResult GetService<TResult>(this IServiceProvider serviceProvider)
        {
            return (TResult)serviceProvider.GetService(typeof(TResult));
        }
    }
}
