﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;

using JmSoft.NetCore.FileLogger;

using Mitto.DataLayer;
using Mitto.Service.Middlewares;

namespace Mitto.Service
{
    public class Startup
    {
        private const string DefaultLogFile = "Mitto.log";

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        ///  This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MittoDbContext>();

            if (Configuration.GetValue<bool>("Testing"))
            {
                services.AddSingleton<PersonRepository>();
                services.AddSingleton<CountryRepository>();
                services.AddSingleton<SMSRepository>();
            }
            else
            {
                services.AddScoped<PersonRepository>();
                services.AddScoped<CountryRepository>();
                services.AddScoped<SMSRepository>();
            }

            services.AddMvc(options =>
                {
                    options.FormatterMappings.SetMediaTypeMappingForFormat
                        ("xml", MediaTypeHeaderValue.Parse("application/xml"));
                    options.FormatterMappings.SetMediaTypeMappingForFormat
                        ("json", MediaTypeHeaderValue.Parse("application/json"));
                })
                .AddXmlDataContractSerializerFormatters()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddRouting();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseMiddleware(typeof(ErrorHandlingMiddleware));
                app.UseStatusCodePages();
                app.UseHsts();
            }

            loggerFactory.AddFile(DefaultLogFile);

            var defaultFile = new DefaultFilesOptions();
            defaultFile.DefaultFileNames.Clear();
            defaultFile.DefaultFileNames.Add("Index.html");
            app.UseDefaultFiles(defaultFile);
            app.UseStaticFiles();
                        
            app.UseMvc(route =>
            {
                route.MapRoute("default", "api/{controller}/{action}");
            });
        }
    }
}
