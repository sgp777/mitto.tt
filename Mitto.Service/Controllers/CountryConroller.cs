﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Mitto.DataLayer;
using Mitto.Models;

namespace Mitto.Service.Controllers
{
    [ApiController]
    [Route("GET/Countries.{format}"), FormatFilter]
    public class CountryConroller : ModelController<Country, CountryRepository>
    {
        [HttpGet]
        public async Task<ActionResult<Country[]>> GetCountries()
        {
            return Ok(await Repository.GetAll());
        }
    }
}