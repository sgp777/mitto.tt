﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Mitto.DataLayer;
using Mitto.Service.Extensions;
using IModel = Mitto.Models.Interfaces.IModel;

namespace Mitto.Service.Controllers
{
    public abstract class ModelController<TModel, TRepository> : ControllerBase 
        where TModel : class, IModel
        where TRepository : ModelRepository<TModel>
    {
        private ILogger _logger;
        public ILogger Logger => _logger ?? (_logger = HttpContext.RequestServices.GetService<ILoggerFactory>().CreateLogger(GetType()));

        public TRepository Repository => HttpContext.RequestServices.GetService<TRepository>();
    }
}
