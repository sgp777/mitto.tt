﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Mitto.DataLayer;
using Mitto.DataLayer.Constants;
using Mitto.Models;
using Mitto.Models.Enums;
using Mitto.Service.Extensions;

namespace Mitto.Service.Controllers
{
    [ApiController]
    [Route("GET/sms"), FormatFilter]
    public class SMSController : ModelController<SMS, SMSRepository>
    {
        private const char MccSeparator = ',';

        public PersonRepository PersonRepository => HttpContext.RequestServices.GetService<PersonRepository>();
        
        [HttpGet("send.{format}")]
        public async Task<ResultState> SendSMS(
            [FromQuery(Name = "from")] string senderName,
            [FromQuery(Name = "to")] string receiverName,
            string text
            )
        {
            var result = ResultState.Success;

            try
            {
                var sms = new SMS
                {
                    Date = DateTime.Now.ToUniversalTime(),
                    Receiver = await PersonRepository.GetPerson(receiverName),
                    Sender = await PersonRepository.GetPerson(senderName),
                    Text = text
                };

                SendMessage(sms);
                await Repository.SaveModel(sms);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error raised during SendSMS execution");
                result = ResultState.Failed;
            }

            return result;
        }

        [HttpGet("sent.{format}")]
        public async Task<ActionResult<SentSMSInfo>> GetSentSMS(
            [FromQuery(Name = "dateTimeFrom")] string dateFromStr,
            [FromQuery(Name = "dateTimeTo")] string dateToStr,
            [FromQuery(Name = "skip")] int skipRecords = DefaultValues.SkipRecords,
            [FromQuery(Name = "take")] int takeRecords = DefaultValues.PageSize
            )
        {
            var dateFrom = DateTime.ParseExact(dateFromStr, DateFormats.DateTime, CultureInfo.CurrentCulture);
            var dateTo = DateTime.ParseExact(dateToStr, DateFormats.DateTime, CultureInfo.CurrentCulture);

            var smsItems = await Repository.GetSentSMS(dateFrom, dateTo, skipRecords, takeRecords);

            return new SentSMSInfo
            {
                TotalCount = smsItems.Count,
                Items = smsItems.Select(m =>
                            new SentSMSInfo.Item
                            {
                                Mcc = m.Receiver.Country.MobileCode,
                                From = m.Sender.Name,
                                To = m.Receiver.Name,
                                Price = m.Receiver.Country.Price
                            }).ToList(),
                State = ResultState.Success
            };
        }

        [Route("/GET/statistics.{format}")]
        [HttpGet]
        public async Task<ActionResult<IList<StatisticItem>>> GetStatistics(
            [FromQuery(Name = "dateFrom")] string dateFromStr,
            [FromQuery(Name = "dateTo")] string dateToStr, 
            [FromQuery(Name = "mccList")] string mccStr)
        {
            IEnumerable<int> mccItems = null;

            var dateFrom = DateTime.ParseExact(dateFromStr, DateFormats.Date, CultureInfo.CurrentCulture);
            var dateTo = DateTime.ParseExact(dateToStr, DateFormats.Date, CultureInfo.CurrentCulture);

            try
            {
                mccItems = mccStr.Split(MccSeparator, StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s));

                if (!mccItems.Any())
                    mccStr = null;
            }
            catch(Exception ex)
            {
                mccStr = null;
                throw new Exception($"Can't convert '{mccStr}' MCC list to list of integers", ex);
            }

            return Ok(await Repository.GetStatisticInfo(dateFrom, dateTo, mccItems));
        }

        private void SendMessage(SMS sms)
        {
            var smsBuilder = new StringBuilder();
            smsBuilder.AppendLine("SMS info:");
            smsBuilder.AppendLine($"Sender - {sms.Sender.Name}");
            smsBuilder.AppendLine($"Receiver - {sms.Receiver.Name}");
            smsBuilder.AppendLine($"MCC - {sms.Receiver.Country.MobileCode}");
            smsBuilder.AppendLine($"Text - {sms.Text}");
            smsBuilder.AppendLine($"***********************\n");

            Logger.LogInformation(smsBuilder.ToString());
        }
    }
}