using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Mitto.Models;
using Mitto.Service.Constants;

namespace Mitto.Tests
{
    [TestClass]
    public class CountryTests : ModelTests<Country>
    {
        [DataRow(RequestFormats.Json)]
        [DataRow(RequestFormats.Xml)]
        [DataTestMethod]
        public async Task GetCountriesTest(string format)
        {
            await ExecTest(format, async client =>
            {
                var url = $"GET/Countries.{format}";
                var countries = await RaiseGetQuery<Country[]>(client, url, format);

                Assert.AreEqual(3, countries.Length);

                Assert.AreEqual(1, countries[0].ID);
                Assert.AreEqual("Germany", countries[0].Name);
                Assert.AreEqual(262, countries[0].MobileCode);
                Assert.AreEqual(49, countries[0].Code);
                Assert.AreEqual(0.055M, countries[0].Price);

                Assert.AreEqual(2, countries[1].ID);
                Assert.AreEqual("Austria", countries[1].Name);
                Assert.AreEqual(232, countries[1].MobileCode);
                Assert.AreEqual(43, countries[1].Code);
                Assert.AreEqual(0.053M, countries[1].Price);

                Assert.AreEqual(3, countries[2].ID);
                Assert.AreEqual("Poland", countries[2].Name);
                Assert.AreEqual(260, countries[2].MobileCode);
                Assert.AreEqual(48, countries[2].Code);
                Assert.AreEqual(0.032M, countries[2].Price);
            });
        }
    }
}
