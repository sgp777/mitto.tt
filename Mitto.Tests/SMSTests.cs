﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Mitto.DataLayer;
using Mitto.DataLayer.Constants;
using Mitto.Models;
using Mitto.Service.Constants;
using Mitto.Models.Enums;
using System.Net;
using Newtonsoft.Json.Linq;

namespace Mitto.Tests
{
    [TestClass]
    public class SMSTests : ModelTests<SMS>
    {
        private const string GermanSender = "Ben";
        private const string GermanReceiver = "Paul";

        private const string AustriaSender = "Tobias";
        private const string AustriaReceiver = "David";

        private const string PolandSender = "Adam";
        private const string PolandReceiver = "Marek";
        
        private async Task InitPersonData()
        {
            var personRep = GetService<PersonRepository>();

            var persons = new List<Person>(new []
            {
                new Person { CountryID = 1, Name = GermanSender },
                new Person { CountryID = 1, Name = GermanReceiver },
                new Person { CountryID = 2, Name = AustriaSender },
                new Person { CountryID = 2, Name = AustriaReceiver },
                new Person { CountryID = 3, Name = PolandSender },
                new Person { CountryID = 3, Name = PolandReceiver }
            });

            await personRep.SaveModels(persons);            
        }

        private async Task InitSmsData(HttpClient client, string format, int maxSmsCount)
        {
            var smsCounter = 1;
            while (smsCounter <= maxSmsCount)
            {
                await SendSMS(client, format, GermanSender, AustriaReceiver, $"From German to Austria :), iteration #{smsCounter}.");
                await SendSMS(client, format, AustriaSender, PolandReceiver, $"From Austria to Poland:), iteration #{smsCounter}.");
                await SendSMS(client, format, PolandSender, GermanReceiver, $"From Poland to German :), iteration #{smsCounter}.");

                smsCounter++;
            }
        }

        private async Task SendSMS(HttpClient client, string format, string sender, string receiver, string text, ResultState expectedResult = ResultState.Success)
        {
            var url = $"GET/sms/send.{format}?from={sender}&to={receiver}&text={text}";
            var result = await RaiseGetQuery<ResultState>(client, url, format);
            Assert.AreEqual(expectedResult, result);
        }

        [DataRow(RequestFormats.Json)]
        [DataRow(RequestFormats.Xml)]
        [DataTestMethod]
        public async Task InvalidPersonTest(string format)
        {
            await ExecTest(format, async client =>
            {
                const string invalidPerson = "Invalid Person";

                await InitPersonData();

                await SendSMS(client, format, invalidPerson, GermanReceiver, "test", ResultState.Failed);
                await SendSMS(client, format, GermanSender, invalidPerson, "test", ResultState.Failed);
            });
        }

        [DataRow(RequestFormats.Json)]
        [DataTestMethod]
        public async Task InvalidDateTest(string format)
        {
            const string invalidDate = "ID777";

            void CheckJsonResult(JObject result)
            {
                Assert.IsTrue(string.Equals(
                    $"String '{invalidDate}' was not recognized as a valid DateTime.",
                    result.Property("error").Value.Value<string>(),
                    StringComparison.OrdinalIgnoreCase));
            }

            await ExecTest(format, async client =>
            {
                // Prepare init data
                await InitPersonData();
                await InitSmsData(client, format, 1);

                // Send GetSentSMS request with invalid date
                var url = $"GET/sms/sent.{format}?dateTimeFrom={invalidDate}&dateTimeTo={invalidDate}";
                var result = await RaiseGetQuery<JObject>(client, url, format, HttpStatusCode.BadRequest);
                CheckJsonResult(result);
                
                // Send GetStatistic request with invalid date
                url = $"GET/statistics.{format}?dateFrom={invalidDate}&dateTo={invalidDate}";
                result = await RaiseGetQuery<JObject>(client, url, format, HttpStatusCode.BadRequest);
                CheckJsonResult(result);
            });
        }

        [DataRow(RequestFormats.Json)] 
        [DataRow(RequestFormats.Xml)]
        [DataTestMethod]
        public async Task GetSentSMSTest(string format)
        {
            await ExecTest(format, async client =>
            {
                // Prepare init data
                const int maxSmsCount = 100;

                await InitPersonData();
                await InitSmsData(client, format, maxSmsCount);

                // Send GetSentSMS request by Date range only and check results
                var dateFrom = DateTime.Now.AddHours(-1).ToUniversalTime().ToString(DateFormats.DateTime);
                var dateTo = DateTime.Now.AddHours(1).ToUniversalTime().ToString(DateFormats.DateTime);

                var url = $"GET/sms/sent.{format}?dateTimeFrom={dateFrom}&dateTimeTo={dateTo}";
                var result = await RaiseGetQuery<SentSMSInfo>(client, url, format);

                Assert.AreEqual(ResultState.Success, result.State);
                Assert.AreEqual(DefaultValues.PageSize, result.TotalCount);

                // Send GetSentSMS request with Date range and Skip parameter and check results
                var skipParam = 270;
                url = $"GET/sms/sent.{format}?dateTimeFrom={dateFrom}&dateTimeTo={dateTo}&skip={skipParam}";
                result = await RaiseGetQuery<SentSMSInfo>(client, url, format);

                var expectedFullCount = maxSmsCount * 3 - skipParam;
                Assert.AreEqual(ResultState.Success, result.State);
                Assert.AreEqual(expectedFullCount < DefaultValues.PageSize ? expectedFullCount : DefaultValues.PageSize, result.TotalCount);

                // Send GetSentSMS request with Date range, Skip and Take parameters and check results
                skipParam = 10;
                const short takeParam = 50;
                url = $"GET/sms/sent.{format}?dateTimeFrom={dateFrom}&dateTimeTo={dateTo}&skip={skipParam}&take={takeParam}";
                result = await RaiseGetQuery<SentSMSInfo>(client, url, format);

                Assert.AreEqual(ResultState.Success, result.State);
                Assert.AreEqual(takeParam, result.TotalCount);
            });
        }

        [DataRow(RequestFormats.Json)]
        [DataRow(RequestFormats.Xml)]
        [DataTestMethod]
        public async Task GetStatisticsTest(string format)
        {
            await ExecTest(format, async client =>
            {
                // Prepare init data
                const int maxSmsCount = 20;

                await InitPersonData();
                await InitSmsData(client, format, maxSmsCount);

                var url = $"GET/Countries.{format}";
                var countries = await RaiseGetQuery<Country[]>(client, url, format);

                // Send GetStatistic request
                var dateFrom = DateTime.Now.ToUniversalTime().Date.ToString(DateFormats.Date);
                var dateTo = DateTime.Now.ToUniversalTime().Date.AddDays(1).ToString(DateFormats.Date);

                url = $"GET/statistics.{format}?dateFrom={dateFrom}&dateTo={dateTo}&mccList={MittoDbContext.GermanMCC},{MittoDbContext.PolandMCC}";
                var result = await RaiseGetQuery<IList<StatisticItem>>(client, url, format);

                // Check results
                Assert.AreEqual(2, result.Count);

                var polandPrice = countries.First(c => c.MobileCode == MittoDbContext.PolandMCC).Price;
                var germanPrice = countries.First(c => c.MobileCode == MittoDbContext.GermanMCC).Price;
                
                Assert.AreEqual(dateFrom, result[0].Day);
                Assert.AreEqual(MittoDbContext.PolandMCC, result[0].Mcc);
                Assert.AreEqual(polandPrice, result[0].PricePerSMS);
                Assert.AreEqual(maxSmsCount, result[0].Count);
                Assert.AreEqual(polandPrice * maxSmsCount, result[0].TotalPrice);

                Assert.AreEqual(dateFrom, result[1].Day);
                Assert.AreEqual(MittoDbContext.GermanMCC, result[1].Mcc);
                Assert.AreEqual(germanPrice, result[1].PricePerSMS);
                Assert.AreEqual(maxSmsCount, result[1].Count);
                Assert.AreEqual(germanPrice*maxSmsCount, result[1].TotalPrice);
            });
        }
    }
}
