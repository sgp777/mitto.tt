﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Transactions;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Web;

using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

using Mitto.Models.Interfaces;
using Mitto.Service;
using Mitto.Service.Constants;

namespace Mitto.Tests
{
    [TestClass]
    public abstract class ModelTests<TModel> where TModel : class, IModel
    {
        private static TestServer Server { get; } = 
            new TestServer(
                WebHost
                .CreateDefaultBuilder()
                .ConfigureAppConfiguration(configBuilder => configBuilder.AddJsonFile("appsettings.Testing.json"))
                .UseStartup<Startup>());
        
        //protected readonly Random Rnd = new Random();

        protected async Task ExecTest(string format, Func<HttpClient, Task> testAction)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var client = CreateHttpClient(format))
                {
                    await testAction(client);
                }
            }
        }

        private HttpClient CreateHttpClient(string format)
        {
            var result = Server.CreateClient();
            result.DefaultRequestHeaders.Accept.Clear();
            result.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue($"application/{format}"));
            
            return result;
        }

        protected async Task<TResult> RaiseGetQuery<TResult>(HttpClient client, string url, string format, HttpStatusCode expectStatusCode = HttpStatusCode.OK)
        {
            var escapedUrl = Uri.EscapeUriString(url);
            var response = await client.GetAsync(escapedUrl);
            return await ProcessResponse<TResult>(response, format, expectStatusCode);
        }

        protected TResult GetService<TResult>()
        {
            return (TResult)Server.Host.Services.GetService(typeof(TResult));
        }

        //protected TResult RaiseGetModel<TResult>(HttpClient client, long modelId, HttpStatusCode expectStatusCode = HttpStatusCode.OK, string modelTypeName = null)
        //{
        //    if (modelTypeName == null)
        //        modelTypeName = typeof(TModel).Name;

        //    var response = client.GetAsync($"api/{modelTypeName}/{modelId}").Result;
        //    return ProcessResponse<TResult>(response, expectStatusCode);
        //}

        //protected long RaiseAddModel(HttpClient client, IModel model, HttpStatusCode expectStatusCode = HttpStatusCode.OK)
        //{
        //    var response = client.PostAsync($"api/{GetModelTypeName(model)}", GetModelContent(model)).Result;
        //    return ProcessResponse<long>(response, expectStatusCode);
        //}

        //protected HttpResponseMessage RaiseUpdateModel(HttpClient client, IModel model, HttpStatusCode expectStatusCode = HttpStatusCode.OK)
        //{
        //    var response = client.PutAsync($"api/{GetModelTypeName(model)}", GetModelContent(model)).Result;
        //    ProcessResponse(response, expectStatusCode);

        //    return response;
        //}

        //protected HttpResponseMessage RaiseDeleteModel(HttpClient client, IModel model, HttpStatusCode expectStatusCode = HttpStatusCode.OK)
        //{
        //    var response = client.DeleteAsync($"api/{GetModelTypeName(model)}/{model.ID}").Result;
        //    ProcessResponse(response, expectStatusCode);

        //    return response;
        //}

        //protected JObject RaiseInvalidQuery(HttpClient client, TModel model)
        //{
        //    var response = RaiseUpdateModel(client, model, HttpStatusCode.BadRequest);

        //    var responseString = response.Content.ReadAsStringAsync().Result;
        //    return JObject.Parse(responseString);
        //}

        //private StringContent GetModelContent(IModel model)
        //{
        //    return new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
        //}

        //private string GetModelTypeName(IModel model)
        //{
        //    return model.GetType().Name;
        //}

        private void ProcessResponse(HttpResponseMessage response, HttpStatusCode expectStatusCode = HttpStatusCode.OK)
        {
            if (response.StatusCode != expectStatusCode)
            {
                var errorContent = $@"
                       Actual status code: {response.StatusCode.ToString()}
                       Expected status code: {expectStatusCode.ToString()}
                       Responce Content: {response.Content.ReadAsStringAsync().Result}";

                Assert.Fail(errorContent);
            }
        }

        private async Task<TResult> ProcessResponse<TResult>(HttpResponseMessage response, string format, HttpStatusCode expectStatusCode = HttpStatusCode.OK)
        {
            ProcessResponse(response, expectStatusCode);
            
            switch (format)
            {
                case RequestFormats.Json:
                    var responseString = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<TResult>(responseString);
                case RequestFormats.Xml:
                    var xmlSerializer = new DataContractSerializer(typeof(TResult));
                    return (TResult)xmlSerializer.ReadObject(await response.Content.ReadAsStreamAsync());
                default:
                    throw new ArgumentException($"Invalid format - {format}");
            }            
        }

        //protected void ProcessTokenValue(JObject json, string tokenPath, string expectedValue)
        //{
        //    string actualValue = null;
        //    var token = json.SelectToken(tokenPath);
        //    if (token is JArray)
        //        actualValue = (string)token.FirstOrDefault();
        //    else if (token is JValue)
        //        actualValue = token.Value<string>();
        //    else
        //        Assert.Fail("Unknown Json Token type");

        //    Assert.IsTrue(string.Equals(actualValue, expectedValue, StringComparison.OrdinalIgnoreCase));
        //}
    }
}
